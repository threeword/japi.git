const fs = require('fs');
const path = require('path');
const parse = require('swagger-parser');
const CodeGen = require('swagger-js-codegen').CodeGen;
const converter = require('api-spec-converter');
const {cmdPath, templatePath, configFileName, configFilePath} = require('../constants/index')

/* 协议转为swagger2 */
const convert = (source, from) => new Promise((resolve, reject) => {
  converter.convert({
    from,
    to: 'swagger_2',
    source,
  }, function (err, swagger) {
    if(err) {
      reject(err)
    } else {
      resolve(JSON.parse(swagger.stringify()))
    }
  })
})

const gen = async (className, opts, apiDir) => {
  try {
    let swagger = {}
    if(opts.spec && opts.spec != 'swagger_2') {
      swagger = await convert(opts.uri, opts.spec)
    } else {
      /* 默认2.0 */
      swagger = await parse.parse(opts.uri);
    }

    /* taro 暂不支持js */
    // const getCode = {
    //   js: CodeGen.getReactCode,
    //   ts: CodeGen.getTypescriptCode
    // }

    let sourceCode = CodeGen.getTypescriptCode({
      className, swagger, lint: false
    });

    /* Taro请求特殊处理 */
    sourceCode = sourceCode
      .replace('import * as request from "superagent";\nimport {\n    SuperAgentStatic\n} from "superagent";'
        , 'type SuperAgentStatic = {};\nconst request = {Response: {}};\nimport {taroRequest} from "./taroRequest";')
      .replace(/this.request/g, 'taroRequest')
      .replace(/private domain/, 'public domain')

    fs.writeFileSync(`${apiDir}/${className}.ts`, sourceCode)
  } catch(e) {
    console.error(className, e);
  }
}

exports.createAPI = async (apis, apiDir) => {
  const indexFormat = []
  indexFormat.push("import { serverConfig } from './config';")

  const compiled = "import { class } from './{ class }'; \n"
    + "export const { name } = new { class }(serverConfig.{ name });"

  const configFormat = []

  for(let i = 0; i < apis.length; i++) {
    const api = apis[i]
    await gen(api.name, api.opts, apiDir);
    const {name, opts: {serverUrl}} = api;
    /* 生成index.ts */
    indexFormat.push(
      compiled.replace(/{ class }/g, name)
        .replace(/{ name }/g, name.charAt(0).toLowerCase() + name.slice(1))
    )
    /* 生成config.ts */
    configFormat.push("  { name }: '{ url }',"
      .replace(/{ name }/g, name.charAt(0).toLowerCase() + name.slice(1))
      .replace(/{ url }/g, serverUrl))
  }

  const apiConfigs = "let serverConfig = {" + '\n' +
    configFormat.join('\n') + '\n' +
    "}" + '\n' +
    "" + '\n' +
    "if (process.env.target == 'dev') {" + '\n' +
    "  Object.assign(serverConfig, {})" + '\n' +
    "}" + '\n' +
    "export {" + '\n' +
    "  serverConfig" + '\n' +
    "}";

  fs.writeFileSync(`${apiDir}/index.js`, indexFormat.join('\n\n'));

  if(!fs.existsSync(`${apiDir}/config.js`)) {
    fs.writeFileSync(`${apiDir}/config.js`, apiConfigs);
  }
}