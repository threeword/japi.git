const fs = require('fs');
const path = require('path');
const parse = require('swagger-parser');
const CodeGen = require('swagger-js-codegen').CodeGen;
const converter = require('api-spec-converter');
const {cmdPath, templatePath, configFileName, configFilePath} = require('../constants/index')

/* 协议转为swagger2 */
const convert = (source, from) => new Promise((resolve, reject) => {
  converter.convert({
    from,
    to: 'swagger_2',
    source,
  }, function (err, swagger) {
    if(err) {
      reject(err)
    } else {
      resolve(JSON.parse(swagger.stringify()))
    }
  })
})

const gen = async (className, opts, apiDir) => {
  try {
    let swagger = {}
    if(opts.spec && opts.spec != 'swagger_2') {
      swagger = await convert(opts.uri, opts.spec)
    } else {
      /* 默认2.0 */
      swagger = await parse.parse(opts.uri);
    }

    const getCode = {
      js: CodeGen.getReactCode,
      ts: CodeGen.getTypescriptCode
    }

    let sourceCode = getCode[opts.type]({
      className, swagger, lint: false
    });

    if(opts.type == 'ts') {
      sourceCode = sourceCode
        .replace('import * as request from "superagent";\nimport {\n    SuperAgentStatic\n} from "superagent";'
          , 'type SuperAgentStatic = {};\nconst request = {Response: {}};\nimport {superagentRequest} from "./superagentRequest";')
        .replace(/this.request/g, 'superagentRequest')
        .replace(/private domain/, 'public domain')
    } else if(opts.type == 'js') {

      const reg = new RegExp(`exports.${className} = ${className};`)

      sourceCode = sourceCode
        .replace("import Q from 'q';"
          , 'import Q from \'q\';\nimport {superagentRequest} from "./superagentRequest";')
        .replace(/this.request/g, 'superagentRequest')
        .replace(/parameters, body, headers, queryParameters, form, deferred\);/g, 'body, headers, queryParameters, form, deferred.reject, deferred.resolve);')
        .replace(reg, `export default ${className};`)
    }

    fs.writeFileSync(`${apiDir}/${className}.${opts.type}`, sourceCode)
  } catch(e) {
    console.error(className, e);
  }
}

exports.createAPI = async (apis, apiDir) => {
  const indexFormat = []
  indexFormat.push("import { serverConfig } from './config';")

  const compiled = "import { class } from './{ class }'; \n"
    + "export const { name } = new { class }(serverConfig.{ name });"

  const configFormat = []

  for(let i = 0; i < apis.length; i++) {
    const api = apis[i]
    await gen(api.name, api.opts, apiDir);
    const {name, opts: {serverUrl}} = api;
    /* 生成index.js */
    indexFormat.push(
      compiled.replace(/{ class }/g, name)
        .replace(/{ name }/g, name.charAt(0).toLowerCase() + name.slice(1))
    )
    /* 生成config.js */
    configFormat.push("  { name }: '{ url }',"
      .replace(/{ name }/g, name.charAt(0).toLowerCase() + name.slice(1))
      .replace(/{ url }/g, serverUrl))
  }

  const apiConfigs = "let serverConfig = {" + '\n' +
    configFormat.join('\n') + '\n' +
    "}" + '\n' +
    "" + '\n' +
    "if (process.env.target == 'dev') {" + '\n' +
    "  Object.assign(serverConfig, {})" + '\n' +
    "}" + '\n' +
    "export {" + '\n' +
    "  serverConfig" + '\n' +
    "}";

  if(!fs.existsSync(`${apiDir}/index.js`)) {
    fs.writeFileSync(`${apiDir}/index.js`, indexFormat.join('\n\n'));
  }

  if(!fs.existsSync(`${apiDir}/config.js`)) {
    fs.writeFileSync(`${apiDir}/config.js`, apiConfigs);
  }
}