import Taro from "@tarojs/taro";
import {onBeforeRequest, onAfterRequest, onErrorRequest} from './hooks';
import queryString from 'querystring';

export const taroRequest = async (
  _method,
  _url,
  _body,
  _headers,
  _queryParameters,
  form = {},
  reject,
  resolve
) => {

  if(_method == "POST") {
    _queryParameters = {
      ..._queryParameters,
      ...form
    };
  }

  let {
    method,
    url,
    body,
    headers,
    queryParameters,
  } = onBeforeRequest({
    method: _method,
    url: _url,
    body: _body,
    headers: _headers,
    queryParameters: _queryParameters,
  }, reject, resolve);

  const queryParams = queryString.stringify(queryParameters)

  const urlWithParams = url + (queryParams ? "?" + queryParams : "");

  if(body && !Object.keys(body).length) {
    body = undefined;
  }

  const requestTask = Taro.request({
    url: urlWithParams,
    method,
    data: body,
    header: headers,
    success(res) {
      onAfterRequest(res, resolve, reject)
    },
    fail(e) {
      onErrorRequest(e, resolve, reject);
    }
  });
};
