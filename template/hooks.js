
/* TODO：实现授权 */
const getToken = () => {
  return ''
}

/** HTTP 状态码 */
const STATUS_CODE = {
  SUCCESS: 200,
  NO_ACCESS: 401
}

/**
 * 请求前
 * @param req 请求信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onBeforeRequest = (req, resolve, reject) => {
  const {method, url, body, headers, queryParameters} = req;
  return {...req, headers: {Authorization: getToken()}};
}

/**
 * 请求返回
 * @param res 返回信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onAfterRequest = (res, resolve, reject) => {
  const {statusCode, header, data, body} = res;
  if(statusCode == STATUS_CODE.SUCCESS) {
    resolve(data || body);
  } else if(statusCode == STATUS_CODE.NO_ACCESS) {
    reject('认证失败');
  } else {
    reject('请求失败');
  }
}

/**
 * 请求失败
 * @param e 错误信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onErrorRequest = (e, resolve, reject) => {
  reject('请求失败');
}
