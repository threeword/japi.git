#!/usr/bin/env node

const ora = require('ora');
const chalk = require('chalk');
const program = require('commander');
const path = require('path');
const fs = require('fs');
const createAPI_Taro = require('../scripts/weapp').createAPI;
const createAPI = require('../scripts/default').createAPI;
const {cmdPath, templatePath, configFileName, configFilePath, apiDir} = require('../constants/index')

/* 生成配置文件 */
const initConfig = () => {
  const spinner = ora(`${configFileName} 生成配置文件...`).start();
  try {

    if(fs.existsSync(configFilePath)) {
      throw `${configFileName} 文件已存在，生成失败！`
    }

    /* 模板内容 */
    const fileContent = fs.readFileSync(path.resolve(templatePath, configFileName));

    /* 写入内容 */
    fs.writeFileSync(configFilePath, fileContent);

    spinner.succeed();
  } catch(e) {
    spinner.fail(e);
  }
}

/* 支持Taro 根据配置协议生成代码 */
const codeGen_Taro = async (a, b) => {

  let _configFilePath = configFilePath;
  let _apiDir = apiDir;

  if(a) {
    _configFilePath = path.resolve(cmdPath, a)
  }

  if(b) {
    _apiDir = path.resolve(cmdPath, b)
  }


  const spinner = ora(`生成API客户端代码...`).start();
  try {
    if(!fs.existsSync(_apiDir)) {
      fs.mkdirSync(_apiDir);
    }

    const hooksFilePath = path.resolve(_apiDir, 'hooks.js')
    if(!fs.existsSync(hooksFilePath)) {
      /* hooks模板内容 */
      const hooksContent = fs.readFileSync(path.resolve(templatePath, 'hooks.js'));
      /* 写入hooks内容 */
      fs.writeFileSync(hooksFilePath, hooksContent);
    }

    const requestFilePath = path.resolve(_apiDir, 'taroRequest.js')
    if(!fs.existsSync(requestFilePath)) {
      /* request模板内容 */
      const requestContent = fs.readFileSync(path.resolve(templatePath, 'taroRequest.js'));
      /* 写入request内容 */
      fs.writeFileSync(requestFilePath, requestContent);
    }

    const {apis} = require(_configFilePath);

    await createAPI_Taro(apis, _apiDir);

    spinner.succeed();
  } catch(e) {
    console.error(e);
    spinner.fail(e);
  }
}

/* 默认superagent 根据配置协议生成代码 */
const codeGen = async (a, b) => {

  let _configFilePath = configFilePath;
  let _apiDir = apiDir;

  if(a) {
    _configFilePath = path.resolve(cmdPath, a)
  }

  if(b) {
    _apiDir = path.resolve(cmdPath, b)
  }

  const spinner = ora(`生成API客户端代码...`).start();
  try {
    if(!fs.existsSync(_apiDir)) {
      fs.mkdirSync(_apiDir);
    }

    const hooksFilePath = path.resolve(_apiDir, 'hooks.js')
    if(!fs.existsSync(hooksFilePath)) {
      /* hooks模板内容 */
      const hooksContent = fs.readFileSync(path.resolve(templatePath, 'hooks.js'));
      /* 写入hooks内容 */
      fs.writeFileSync(hooksFilePath, hooksContent);
    }

    const requestFilePath = path.resolve(_apiDir, 'superagentRequest.js')
    if(!fs.existsSync(requestFilePath)) {
      /* request模板内容 */
      const requestContent = fs.readFileSync(path.resolve(templatePath, 'superagentRequest.js'));
      /* 写入request内容 */
      fs.writeFileSync(requestFilePath, requestContent);
    }

    const {apis} = require(_configFilePath);

    await createAPI(apis, _apiDir);

    spinner.succeed();
  } catch(e) {
    console.error(e);
    spinner.fail(e);
  }
}

program
  .command('init')
  .description('生成配置文件')
  .action(initConfig);

program
  .command('create [configFile] [apisDir]')
  .description('根据配置协议生成代码，使用superagent。[configFile: 配置文件路径], [apisDir: 生成路径]')
  .action(codeGen);

program
  .command('create:taro [configFile] [apisDir]')
  .description('根据配置协议生成代码，使用Taro.request。[configFile: 配置文件路径], [apisDir: 生成路径]')
  .action(codeGen_Taro);

program
  .version(require('../package').version)
  .parse(process.argv);