exports.apis = [
  {
    /** 接口命名 */
    name: 'ServicePublic',
    /** 配置 */
    opts: {
      /** api协议地址 支持 swagger_1 | swagger_2 | openapi_3 | api_blueprint | io_docs | google | raml | wadl */
      uri: 'https://yourswagger.com/swagger.json',
      /** 协议类型 默认 swagger_2 */
      spec: 'swagger_2',
      /** 需要生成文件类型 支持 js | ts */
      type: 'ts',
      /** api接口服务地址 也可在config.js中 配置开发环境、测试环境、生成环境 */
      serverUrl: 'https://yourswagger.com'
    }
  },
  /** 自行补充其他协议配置 结构如上 */
];