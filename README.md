# japi

#### 介绍

前端 API 集成、通过 API 协议生成前端代码（兼容 [Taro](https://taro.aotu.io/)）

#### 前言

> **有没有写过这种代码。如下图**

![](https://images.gitee.com/uploads/images/2020/0610/110009_7792193c_5505160.png)

> **有没有感受过一丝丝头疼**

> **接口协议变更时、接口服务器地址变更时、各种环境来回切换时有没有感受过一丝丝恶心**

> **好了我就是来解决这个问题的**

#### 安装教程

```bash
# 本地安装
npm i @threeword/japi -D


# 或者 安装至全局
npm i @threeword/japi -g
```

#### 使用说明

### 第一步：初始化接口配置

```bash
npx japi init
```

会在根目录下生成 apiConfig.js
``` js
/** apiConfig.js */

exports.apis = [
  {
    /** 接口命名（注意大写开头，会作为Class的名称） */
    name: 'ServicePublic',
    /** 配置 */
    opts: {
      /** api协议地址 */
      uri: '',
      /** 协议类型 (swagger_1 | swagger_2 | openapi_3 | api_blueprint | io_docs | google | raml | wadl) */
      spec: 'swagger_2',
      /** 需要生成文件类型 支持 js | ts (注意：Taro项目自带ts天赋，所以只支持ts) */
      type: 'ts',
      /** api接口服务地址 也可在config.js中 配置开发环境、测试环境、生成环境 */
      serverUrl: ''
    }
  },
  /** 自行补充其他协议配置 结构如上 */
];
```

###### 配置说明
- name : 生成的文件名字，也是Class的名称（注意：和Class一样大写开头）
- opts.uri : api协议地址
- opts.spec : 协议类型（swagger_1 | swagger_2 | openapi_3 | api_blueprint | io_docs | google | raml | wadl）
- opts.type : 生成文件类型（ts | js）
- opts.serverUrl : 接口服务器地址

### 第二步：生成客户端代码

```bash

/** 第一种 基于supperagent #1 */
npx japi create

/** 第二种 基于Taro.request（应用于Taro项目）#2 */
npx japi create:taro

/** Taro项目生成客户端代码 建议放到src目录下 命令修改如下 #2 */
npx japi create:taro ./apiConfig.js ./src/apis
```

生成apis文件夹，如图

![](https://images.gitee.com/uploads/images/2020/0610/101028_5039b20f_5505160.png)

- config.js : 配置接口服务地址（可区分：开发环境、生产环境、测试环境）
- hooks.js : 请求周期钩子（onBeforeRequest: 请求前（填充token），onAfterRequest: 请求返回（处理状态码），onErrorRequest: 请求出错（reject））
- index.js : index文件
- [ServicePublic.ts] : 根据apiConfig配置的生产的接口代码
- [superagentRequest.js  #1] : superagent 只有 "npx japi create" 生成
- [taroRequest.js  #2] : Taro.request 只有 "npx japi create:taro" 生成


### 第三步：调用接口

调用接口就特别简单了

```
import { servicePublic } from 'src/apis';

const res = await servicePublic.GetArea({});
console.log(res);
```

#### 补充说明

1.  当存在config.js，hooks.js，"create"指令不会进行覆盖，请放心更改config.js和hooks.js。
2.  其他文件会根据协议再次生成覆盖（请不要轻易更改），如出现问题请提issue。
3.  Taro项目自带ts天赋，所以只支持ts的，即opts.type配置无效。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
